<?php include_once('requires/head.php');?>
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <?php include_once('requires/header.php'); ?>
    <?php include_once('requires/leftmenu.php'); ?>
    <div class="page-wrapper">
            <div class="container-fluid">
               <div class="card">
                   <div class="card-heading">
                      <h2 class="box-title">User Information</h2>
                   </div>
                   <div class="card-body">
                    <form class="form-horizontal form-material" method="POST" action="">
                      <div class="row">
                        <!-- Column -->
                        <div class="col-lg-4 col-xlg-3 col-md-12 imgUp">       
                            <div class="imagePreview"></div>
                            <label style="width: 80%;" class="text-center btn btn-primary mt-2">
                                Upload Profile Picture
                            <input type="file" name="user_image" class="uploadFile img" placeholder="Upload Photo" style="display:none; overflow: hidden;">
                            </label>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-8 col-xlg-9 col-md-12">
                          <div class="row">
                              <div class="col-lg-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Full Name</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input type="text" placeholder=""
                                                class="form-control p-0 border-0"> </div>
                                    </div>
                                  
                                    <div class="form-group mb-4">
                                        <label class="col-sm-12">Year of Completion</label>
    
                                        <div class="col-sm-12 border-bottom">
                                            <select id="ddlYears" class="form-select shadow-none p-0 border-0 form-control-line">
                                                <!-- <option>Year of Completion</option> -->
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Email</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input type="text" placeholder=""
                                                class="form-control p-0 border-0">
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Bio</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <textarea rows="2" class="form-control p-0 border-0"></textarea>
                                    </div>
                                </div>
                                 </div>
                                    
                              <div class="col-lg-6">
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Nickname</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="text" placeholder=""
                                            class="form-control p-0 border-0"> </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12">Department</label>

                                    <div class="col-sm-12 border-bottom">
                                        <select class="form-select shadow-none p-0 border-0 form-control-line">
                                            <!-- <option value="0">Department</option> -->
                                            <option value="ACF">Accounting & Finance</option>
                                            <option value="HROD">Human Resources & Organisational Development</option>
                                            <option value="MCS">Marketing & Corporate Strategy</option>
                                            <option value="SCIS">Supply Chain & Information Systems</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Student Number</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="text" placeholder=""
                                            class="form-control p-0 border-0"> 
                                    </div>
                                </div>
                               
                          </div>

                          <div class="form-group mb-4">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-success float-right">Add User</button>
                            </div>
                          </div>
                    </div>
               </div>
                      </div>
                    </form>
                   </div>
                </div>
           </div>
               
            </div>
            <?php include_once('requires/footer.php'); ?>
        </div>
    </div>
    <?php include_once('requires/scripts.php'); ?>
</body>

</html>