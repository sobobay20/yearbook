<?php include_once('requires/head.php');?>
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <?php include_once('requires/header.php'); ?>
    <?php include_once('requires/leftmenu.php'); ?>
        <div class="page-wrapper" style="min-height: 250px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Blank Page</h3>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once('requires/footer.php'); ?>
        </div>
    </div>
    <?php include_once('requires/scripts.php'); ?>
</body>

</html>