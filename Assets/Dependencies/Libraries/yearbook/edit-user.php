<!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Main Page | Year Book- KSB</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>        
    <link rel="stylesheet" href="css/dropzone.css" />
    <link href="signup/css/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>
    <link href="signup/css/img_crop.css" rel="stylesheet"/>
    <link href="assets/css/all.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <header id="header" class="d-flex align-items-center">
            <div class="container d-flex align-items-center">
            <a href="main.php" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>
            <nav id="navbar" class="navbar">
                <ul>
                    <li>
                    <div class="dropdown">
                                <a style="cursor: pointer;" class="dropdown" data-toggle="dropdown">
                                    <img src="admin/assets/plugins/images/users/varun.jpg" style="border-radius: 50%;" alt="user-img" width="36" class="img-circle">
                                    <span style="color: #706e6e" class="p-2"> Welcome, Steave</span>
                                </a>
                                <ul class="dropdown-menu p-3" style="cursor: pointer;" role="menu" aria-labelledby="menu1">
                                    <a class="p-1" style="color: #706e6e" href="edit-user.php">
                                        <li role="presentation"><i class="fa fa-user"></i> 
                                           <span class="p-1">Profile</span>
                                        </li>
                                    </a>
                                    <a class="p-1" style="color: #706e6e" href="logout.php">
                                        <li role="presentation"><i class="fa fa-sign-in-alt"></i>
                                          <span class="p-1">Logout</span>
                                        </li>
                                    </a>
                                </ul>
                              </div>
                   </li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
            </div>
        </header>

        <main id="main">
           <section id="search" class="search-bg">
              <div class="container">
                <div class="card">
                    <div class="card-header">
                       <h3 class="card-title">
                           <i class="fas fa-user-cog"></i>
                           Profile Settings
                       </h3>
                    </div>
                    <div class="card-body">
                    <?php echo $msg; ?>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Almost there!</strong> 
                        You should complete your profile before proceeding.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="row p-5">
                            <div class="col-md-4 col-sm-12">
                               <div class="image_area">      
                                    <label for="upload_image">
                                        <img src="assets/img/team/team-2.jpg" id="uploaded_image" class="img-responsive img-circle" />
                                        <div class="overlay">
                                            <div class="text">Change Profile Picture</div>
                                        </div>
                                        <input type="file" name="user_image" class="image" id="upload_image" style="display:none" />
                                    </label>

                                </div>  
                            </div>
                            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"  aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Crop Image Before Upload</h5>
                                        <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="img-container">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <img src="" id="sample_image" />
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="preview"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="crop" class="btn btn-primary">Crop</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    </div>
                              </div>
                            </div>
                          </div>
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                            <input type="email" class="form-input" name="user_email" placeholder="Enter Your Email"/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-input" name="fullmame" placeholder="Enter Your Full Name"/>
                        </div>
                      
                        <div class="form-group">
                            <select class="form-input" name="year_completed" id="ddlYears">
                                <option value="year">Select Year of Completion</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-input" name="user_bio"  cols="2" rows="2">Bio</textarea>
                        </div>
                        
                            </div>
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                            <input type="text" class="form-input" name="nickname" id="nickname" placeholder="Your Nickname"/>
                            </div>
                            <div class="form-group">
                            <input type="text" class="form-input" name="student_no" id="nickname" placeholder="Your Student Number"/>
                            </div>
                            <div class="form-group">
                            <select class="form-input" name="department">
                                <option value="year">Select Department</option>
                                <option value="year">Accounting & Finance</option>
                                <option value="year">Human Resource & Organizational Development </option>
                                <option value="year">Marketing & Corporate Strategy</option>
                                <option value="year">Supply Chain & Information Systems</option>
                            </select>
                            </div>
                            <!-- <div class="form-group">
                            <input type="text" class="form-input" name="nickname" id="nickname" placeholder="Your Nickname"/>
                            </div> -->
                            <div class="form-group">
                            <input type="submit" name="signup"  class="form-submit" value="Update Profile"/>
                            </div>
                            </div>
                          
                        </div>
                       
                    </div>
                </div>
              </div>
        </section>
    </main>
    <footer id="footer">
        <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>Green</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
        </div>
    </footer>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <!-- <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/js/all.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="signup/js/main.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){

        var $modal = $('#modal');

        var image = document.getElementById('sample_image');

        var cropper;

        $('#upload_image').change(function(event){
            var files = event.target.files;

            var done = function(url){
                image.src = url;
                $modal.modal('show');
            };

            if(files && files.length > 0)
            {
                reader = new FileReader();
                reader.onload = function(event)
                {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 3,
                preview:'.preview'
            });
        }).on('hidden.bs.modal', function(){
            cropper.destroy();
            cropper = null;
        });

        $('#crop').click(function(){
            canvas = cropper.getCroppedCanvas({
                width:400,
                height:400
            });

            canvas.toBlob(function(blob){
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function(){
                    var base64data = reader.result;
                    $.ajax({
                        url:'upload.php',
                        method:'POST',
                        data:{image:base64data},
                        success:function(data)
                        {
                            $modal.modal('hide');
                            $('#uploaded_image').attr('src', data);
                        }
                    });
                };
            });
        });
        
    });
    </script>
    <script type="text/javascript">
        window.onload = function () {
            //Reference the DropDownList.
            var ddlYears = document.getElementById("ddlYears");
    
            //Determine the Current Year.
            var currentYear = (new Date()).getFullYear();
    
            //Loop and add the Year values to DropDownList.
            for (var i = 2005; i <= currentYear; i++) {
                var option = document.createElement("OPTION");
                option.innerHTML = i;
                option.value = i;
                ddlYears.appendChild(option);
            }
        };
    </script>
    </body>
</html>