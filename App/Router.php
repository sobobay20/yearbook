<?php

/*
*
* Register all routes here
*
*
* Query - query string
* Use - file folder or location. Make changes to it in the App/Config.php file
* AsDefault - sets the default page. eg. login/home
* Call - invoke the fil
* Boot - runs the script
*
*
* 
*/


/*
*
* route to the Home Page
*
*/
Router::Query('home')->Use($VisitorsProcessDir)->AsDefault()->Call('Home.php');
/*
*
* route to the Login Page
*
*/
Router::Query('login')->Use($VisitorsProcessDir)->Call('Login.php');
/*
*
* route to the Sign Up Page
*
*/
Router::Query('signup')->Use($VisitorsProcessDir)->Call('Signup.php');
/*
*
* route to the Forgot Password Page
*
*/
Router::Query('forgot')->Use($VisitorsProcessDir)->Call('Forgot.php');
/*
*
* route to the Main Page
*
*/
Router::Query('main')->Use($VisitorsProcessDir)->Call('Main.php');
/*
*
* route to the Admin Profile & Edit Users Page
*
*/
Router::Query('edit')->Use($AdminProcessDir)->Call('Edit.php');
/*
*
* route to the User Profile Page
*
*/
Router::Query('update')->Use($VisitorsProcessDir)->Call('Update.php');
/*
*
* route to the Admin Home Page
*
*/
Router::Query('admin')->Use($AdminProcessDir)->Call('Home.php');
/*
*
* route to the Home Page
*
*/
Router::Query('pending')->Use($AdminProcessDir)->Call('Pending.php');
/*
*
* route to the Home Page
*
*/
Router::Query('approved')->Use($AdminProcessDir)->Call('Approved.php');
/*
*
* route to the Home Page
*
*/
Router::Query('newuser')->Use($AdminProcessDir)->Call('NewUser.php');
/*
*
* route to the Home Page
*
*/
Router::Query('search')->Use($VisitorsProcessDir)->Call('SearchResults.php');

// Router::Query('logout')->Use($AuthProcessDir)->Call('Logout.php');

/*
*
* route to the dashboard folder
*
*/

//Router::Query('auth/dashboard/home')->Use($AuthProcessDir)->Call('Admin/Dashboard/AdminHome.php');

/*
*
*
* Boot the route. Comment if not want to view
*
*
*/

Router::Boot();



		   
