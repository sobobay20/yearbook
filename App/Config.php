<?php

/*
*
* Default time zone
*
*
*/

date_default_timezone_set("Africa/Accra");



/*
*
* Application Details
*
*/

$AppName = "Year Book-KSBSA";

$Copyright = "&copy 2021 | Year Book-KSBSA";

$BaseURL = "?";

//$FetchLimit = 1;


/*
*
* Database Connection Details
*
*/

$DatabaseHost = "127.0.0.1";

$DatabaseUserName = "root";

$DatabaseName = "ksb_yearbook";

$DatabasePassword = "";



/*
*
* Database Table Names
*
*/

$_UsersTable = "users_table";

$_PrivilegesTable = "privileges";

$_AboutTable = "about_us";

$_NewsTable = "news";

$_CommentsTable = "comments";



/*
*
* Folder Directories. Kindly register new directories here
* Classes Directories
*
*/


$ClassesDir = "App/Classes";



/*
*
* Assets Folder Directories
*
*/


$LibrariesDir = "Assets/Dependencies/Libraries";

$CSSDir = "Assets/Dependencies/SystemCSS";

$JSDir = "Assets/Dependencies/SystemJS";

$DocumentsDir = "Assets/Media/Documents";

$ImagesDir = "Assets/Media/Images";

$UsersImagesDir = "Assets/Media/Images/UsersImages";

$VideosDir = "Assets/Media/Videos";



/*
*
* Processors Folder Directories
*
*/

$AuthProcessDir = "Public/Process/Auth/";
$VisitorsProcessDir = "Public/Process/Visitors/";
$AdminProcessDir = "Public/Process/Admin/";


/*
*
* Views Folder Directories
*
*/

$VisitorsViewsDir = "Public/Views/Visitors";
$AdminViewsDir = "Public/Views/Admin";
$AuthViewsDir = "Public/Views/Auth";



/*
*
* Includes Folder Directories
*
*/


$IncludesDir = "Public/Views/Includes";



			   
