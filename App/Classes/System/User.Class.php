<?php 


class User
{

    private $Table = "";
    private $Passport = "";
    private  $CompanyLogoDestination = "Assets/Media/Images/UsersImages/";
    

    public function __construct($Table)

    {

        $this->Table = $Table;
        $this->Passport = new Passport();

    }


    private function ValidateStudentNo()

    {

        $this->Passport->Use("StudentNo")
                        ->Required("Student Number is required !")
                        ->Regex("/[a-zA-Z0-9 ]/", "Sorry we only accept a-zA-Z0-9 and white space");

    }

    private function ValidateStudentEmail()

    {

        $this->Passport->Use("StudentEmail")
                        ->Required("Student Email is required !")
                        ->Mail("Input Correct Email")
                        ->Regex("/[a-zA-Z0-9 ]/", "Sorry we only accept a-zA-Z0-9 and white space");

    }

    private function ValidateStudentPassword()

    {

        $this->Passport->Use("StudentPassword")
                        ->Required("Password is required !")
                        ->Regex("/[0-9]/", "Sorry we only accept only 0-9 . Type a correct phone number");

    }

    private function ValidateStudentFirstName()

    {

        $this->Passport->Use("StudentFirstName")
                        ->Required("First Name is required !")
                        ->Regex("/[a-zA-Z0-9 ]/", "Sorry we only accept a-zA-Z0-9 and white space");

    }


    private function ValidateStudentOtherNames()

    {

        $this->Passport->Use("StudentOtherNames")

                        ->Regex("/[a-zA-Z0-9 ]/", "Sorry we only accept a-zA-Z0-9 and white space");

    }

    private function ValidateStudentNickname()

    {

        $this->Passport->Use("StudentNickname")
                        ->Regex("/[a-zA-Z0-9 ]/", "Sorry we only accept a-zA-Z0-9 and white space");

    }

    private function ValidateStudentBio()

    {

        $this->Passport->Use("StudentBio")
                       ->Regex("/[a-zA-Z0-9 ]/", "Sorry we only accept a-zA-Z0-9 and white space");


    }

    private function ValidateYearCompleted()

    {

        $this->Passport->Use("YearCompleted")
                        ->Required("Year of Completion is required !");

    }

    private function ValidateDepartment()

    {

        $this->Passport->Use("Department")
                        ->Required("Department is required !");

    }

    private function ValidateStudentImage()

	{
		$this->Passport->Use("StudentImage")
					   ->Image("StudentImage",$this->RenameFile());
	}

    public function AddUser()
    {
        $this->ValidateStudentNo();
        $this->ValidateStudentEmail();
        $this->ValidateStudentPassword();
        $this->ValidateStudentFirstName();
        $this->ValidateStudentOthernames();
        $this->ValidateStudentNickname();
        $this->ValidateStudentBio();
        $this->ValidateYearCompleted();
        $this->ValidateDepartment();
        $this->ValidateStudentImage();

        if (!$this->Passport->AnyErrors())
        {
            $Data = $this->Passport->Output();
        
            $this->Table->Columns( array_keys($Data) )->Values( array_values($Data) )->Insert();
            return 1;
        }
        else
        {
            return $this->Passport->Errors();
        }
    }


    public function UpdateUser($UserID)

    {

        $this->ValidateStudentNo();
        $this->ValidateStudentEmail();
        $this->ValidateStudentPassword();
        $this->ValidateStudentFirstName();
        $this->ValidateStudentOthernames();
        $this->ValidateStudentNickname();
        $this->ValidateStudentBio();
        $this->ValidateYearCompleted();
        $this->ValidateDepartment();
        if($_FILES['StudentImage']['name'] != ""){  $this->ValidateCompanyLogo(); }

        if (!$this->Passport->AnyErrors())
        {
            $Data = $this->Passport->Output();
            $Condition = ["UserId"=>$CompanyID];
            $this->Table->Columns( array_keys($Data) )->Values( array_values($Data) )->Where($Condition)->Update();
            return 1;
        }
        else
        {
            return $this->Passport->Errors();
        }

    }

    
    public function GetUserByID($UserID)
    {
        $Condition = ["UserId"=>$UserID];

        return $this->Table->Where( $Condition )->Get()[0];

    }


    public function DeleteUser($UserID)

    {
        $Condition = ["UserId"=>$CompanyID];
        $Data = ["IsDeleted"=>1, "UpdatedAt"=>Date::DateTimeNow()];

        return $this->Table->Columns(array_keys($Data))->Values(array_values($Data))->Where( $Condition )->Update();

    }


    public function List()

    {
        $Condition = ["IsDeleted"=>0];
        return $this->Table->Where($Condition)->Get();

    }


    public function UserDetailsFor($UserID)

    {

        $Condition = ["UserId"=>$UserID];
        return $this->Table->Where($Condition)->Get();

    }



    private function RenameFile()

	{
		$name_arr = explode(".", $_FILES['StudentImage']['name'] );

		$extension= strtolower(end( $name_arr ));

		if($extension == "jpg" || $extension == "jpeg" || $extension == "png")

		{
			$new_file_name = strtolower( date('Y-m-d').CodeGenerator::Generate(20).".".$extension );

			$Destination = $this->ImageDestination.''.basename($new_file_name);

			return $Destination;

		}

		if($_FILES['StudentImage']['name'] != "")

		{
			$this->Passport->AddError("Upload Only JPG and PNG as Profile Picture");

		}
		
	}

}


    ?>