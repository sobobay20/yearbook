
    <div class="page-wrapper">
            <div class="container-fluid">
               <div class="card">
                   <div class="card-heading">
                      <h2 class="box-title">Edit User Information</h2>
                   </div>
                   <div class="card-body">
                    <form class="form-horizontal form-material">
                      <div class="row">
                        <!-- Column -->
                        <div class="col-md-4 col-sm-12 imgUp">
                             <div class="imagePreview" style="background:url('<?=$ImagesDir?>/SystemImages/team/team-2.jpg'); background-position: center center; background-color:#fff; background-size: cover; background-repeat:no-repeat; border-radius: 50%; height: 300px;"></div>  
                             <label class="btn btn-primary mt-2" style="width: 80%;">
								Update Profile picture<input type="file" class="uploadFile img" value="Upload Profile Picture" style="display: none;">
				             </label>
                        </div>
                            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"  aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Crop Image Before Upload</h5>
                                        <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="img-container">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <img src="" id="sample_image" />
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="preview"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="crop" class="btn btn-primary">Crop</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    </div>
                              </div>
                            </div>
                          </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-8 col-xlg-9 col-md-12">
                          <div class="row">
                              <div class="col-lg-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Full Name</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input type="text" value="Johnathan Doe"
                                                class="form-control p-0 border-0"> </div>
                                    </div>
                                  
                                    <div class="form-group mb-4">
                                        <label class="col-sm-12">Year of Completion</label>
    
                                        <div class="col-sm-12 border-bottom">
                                            <select id="ddlYears" class="form-select shadow-none p-0 border-0 form-control-line">
                                                <!-- <option>Year of Completion</option> -->
                                            </select>
                                        </div>
                                    </div>
                                   
                                    <!-- <div class="form-group mb-4">
                                        <label class="col-sm-12">Security Questions</label>
    
                                        <div class="col-sm-12 border-bottom">
                                            <select class="form-select shadow-none p-0 border-0 form-control-line">
                                                <option>Security Question</option>
                                                <option>What is your Birthdate?</option>
                                                <option>What is Your old Phone Number</option>
                                                <option>What is your Pet Name?</option>
                                            </select>
                                        </div>
                                    </div> -->
                                   
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Username</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input type="text" value="kofidadzie"
                                                class="form-control p-0 border-0">
                                        </div>
                                    </div>
                                 </div>
                                    
                              <div class="col-lg-6">
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Nickname</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="text" value="Jonah"
                                            class="form-control p-0 border-0"> </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12">Department</label>

                                    <div class="col-sm-12 border-bottom">
                                        <select class="form-select shadow-none p-0 border-0 form-control-line">
                                            <!-- <option value="0">Department</option> -->
                                            <option value="ACF">Accounting & Finance</option>
                                            <option value="HROD">Human Resources & Organisational Development</option>
                                            <option value="MCS">Marketing & Corporate Strategy</option>
                                            <option value="SCIS">Supply Chain & Information Systems</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Security Answer</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="text" value="Johnathan Doe"
                                            class="form-control p-0 border-0"> 
                                    </div>
                                </div> -->
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Bio</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <textarea rows="2" class="form-control p-0 border-0"></textarea>
                                    </div>
                                </div>
                          </div>

                          <div class="form-group mb-4">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-success float-right">Update Profile</button>
                            </div>
                          </div>
                    </div>
               </div>
                      </div>
                    </form>
                   </div>
                </div>
           </div>
               
            </div>
  