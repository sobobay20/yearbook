<header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center">
    <a href="<?=$BaseURL;?>/home" class="logo me-auto"><img src="<?=$ImagesDir?>/SystemImages/logo.png" alt="" class="img-fluid"></a>
    <nav id="navbar" class="navbar">
        <ul>
            <li><a class="getstarted scrollto" href="<?=$BaseURL;?>/login">Login</a></li>
            <li><a class="getstarted scrollto" href="<?=$BaseURL;?>/signup">Sign Up</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
    </nav>
    </div>
</header>