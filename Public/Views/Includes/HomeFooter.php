<footer id="footer">
        <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>Green</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
        </div>
    </footer>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <script src="<?=$LibrariesDir;?>/yearbook/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/assets/vendor/php-email-form/validate.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/assets/js/all.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/assets/js/main.js"></script>
    <script type="text/javascript">
        window.onload = function () {
            var ddlYears = document.getElementById("ddlYears");
            var currentYear = (new Date()).getFullYear();
            for (var i = 2005; i <= currentYear; i++) {
                var option = document.createElement("OPTION");
                option.innerHTML = i;
                option.value = i;
                ddlYears.appendChild(option);
            }
        };
    </script>
    </body>
</html>