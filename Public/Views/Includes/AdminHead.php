<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Editport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Ample lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Ample admin lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Ample Admin Lite is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title><?=$Title?></title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/ample-admin-lite/" />
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$ImagesDir?>/SystemImages/favicon.png">
    <link href="<?=$LibrariesDir;?>/yearbook/admin/assets/css/bootstrap4.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/admin/assets/css/datatable.bootstrap4.min.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/admin/assets/plugins/bower_components/chartist/dist/chartist.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$LibrariesDir;?>/yearbook/admin/assets/plugins/bower_components/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css">
    <link href="<?=$LibrariesDir;?>/yearbook/admin/assets/css/avartar.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/admin/assets/css/style.min.css" rel="stylesheet">
   
</head>
<body>
   