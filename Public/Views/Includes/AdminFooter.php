<footer class="footer text-center"> 2021 © Ample Admin brought to you by <a
                    href="https://www.wrappixel.com/">wrappixel.com</a>
</footer>
        </div>
    </div>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/app-style-switcher.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/waves.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/sidebarmenu.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/bootstrap.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/jquery.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/plugins/bower_components/chartist/dist/chartist.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/plugins/bower_components/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/pages/dashboards/dashboard1.js"></script>   
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/jquery.datatables.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/datatables.bootstrap.min.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/avartar.js"></script>
    <script src="<?=$LibrariesDir;?>/yearbook/admin/assets/js/custom.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable();
        });
    </script>
    <script type="text/javascript">
        window.onload = function () {
            //Reference the DropDownList.
            var ddlYears = document.getElementById("ddlYears");
    
            //Determine the Current Year.
            var currentYear = (new Date()).getFullYear();
    
            //Loop and add the Year values to DropDownList.
            for (var i = 2005; i <= currentYear; i++) {
                var option = document.createElement("OPTION");
                option.innerHTML = i;
                option.value = i;
                ddlYears.appendChild(option);
            }
        };
    </script>
</body>

</html>