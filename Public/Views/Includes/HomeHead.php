<!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Year Book- KSB</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link href="<?=$ImagesDir?>/SystemImages/favicon.png" rel="icon">
    <!-- <link href="<?=$ImagesDir?>/SystemImages/apple-touch-icon.png" rel="apple-touch-icon"> -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/css/all.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/css/style.css" rel="stylesheet">
  </head>
  <body>