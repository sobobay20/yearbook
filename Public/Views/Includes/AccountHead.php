<!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title><?=$Title?></title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link href="<?=$ImagesDir?>/SystemImages/favicon.png" rel="icon">
    <link href="<?=$ImagesDir?>/SystemImages/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="<?=$LibrariesDir;?>/yearbook/assets/css/bootstrap.css">
    <!-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> -->
    <link href="<?=$LibrariesDir;?>/yearbook/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$LibrariesDir;?>/yearbook/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>        
    <link rel="stylesheet" href="<?=$LibrariesDir;?>/yearbook/assets/css/dropzone.css" />
    <link href="<?=$LibrariesDir;?>/yearbook/assets/css/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>
    <link href="<?=$LibrariesDir;?>/yearbook/assets/css/img_crop.css" rel="stylesheet"/>
    <!-- <link href="<?=$LibrariesDir;?>/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet"/> -->
     <!-- Main css -->
     <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon.png">
    <link rel="stylesheet" href="<?=$LibrariesDir;?>/yearbook/login/css/style.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>

    </head>
    <body>