<div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

<header class="topbar" data-navbarbg="skin5">
<nav class="navbar top-navbar navbar-expand-md navbar-dark">
    <div class="navbar-header" data-logobg="skin6">
        <a class="navbar-brand" href="<?=$BaseURL;?>/admin">
            <b class="logo-icon">
                <img src="<?=$ImagesDir?>/SystemImages/logo.png" alt="homepage" style="width: 200px;" />
            </b>
        </a>
        <a class="nav-toggler waves-effect waves-light text-dark d-block d-md-none"
            href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
    </div>
    <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
        <ul class="navbar-nav ms-auto d-flex align-items-center">
            <li>
            <div class="dropdown">
                        <a class="dropdown-toggle profile-pic" type="button" id="menu1" data-toggle="dropdown">
                            <img src="<?=$ImagesDir?>/SystemImages/users/varun.jpg" alt="user-img" width="36"
                            class="img-circle">
                            <span class="text-white font-medium">Welcome, Steave</span></a>
                        </a>
                        <ul class="dropdown-menu p-3" style="cursor: pointer;" role="menu" aria-labelledby="menu1">
                            <a class="p-1" style="color: #706e6e" href="<?=$BaseURL;?>/edit">
                                <li role="presentation"><i class="fas fa-user"></i> Profile</li>
                            </a>
                            <a class="p-1" style="color: #706e6e" href="<?=$BaseURL;?>/settings">
                                <li role="presentation"><i class="fas fa-cog"></i> Settings</li>
                            </a>
                            <a class="p-1" style="color: #706e6e" href="<?=$BaseURL;?>/logout">
                                <li role="presentation"><i class="fas fa-sign-in-alt"></i> Logout</li>
                            </a>
                        </ul>
                        </div>
            </li>
            
        </ul>
    </div>
</nav>
</header>