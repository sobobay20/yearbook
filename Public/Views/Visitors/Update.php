<main id="main">
    <section id="search" class="search-bg">
        <div class="container">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fas fa-user-cog"></i>
                    Profile Settings
                </h3>
            </div>
            <div class="card-body">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Almost there!</strong> 
                You should complete your profile before proceeding.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="row p-5">
                    <div class="col-md-4 col-sm-12">
                        <div class="image_area">      
                            <label for="upload_image">
                                <img src="<?=$ImagesDir?>/SystemImages/team/team-2.jpg" id="uploaded_image" class="img-responsive img-circle" />
                                <div class="overlay">
                                    <div class="text">Change Profile Picture</div>
                                </div>
                                <input type="file" name="user_image" class="image" id="upload_image" style="display:none" />
                            </label>

                        </div>  
                    </div>
                    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"  aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Crop Image Before Upload</h5>
                                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="img-container">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <img src="" id="sample_image" />
                                        </div>
                                        <div class="col-md-4">
                                            <div class="preview"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="crop" class="btn btn-primary">Crop</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                    <input type="email" class="form-input" name="user_email" placeholder="Enter Your Email"/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-input" name="fullmame" placeholder="Enter Your Full Name"/>
                </div>
                
                <div class="form-group">
                    <select class="form-input" name="year_completed" id="ddlYears">
                        <option value="year">Select Year of Completion</option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea class="form-input" name="user_bio"  cols="2" rows="2">Bio</textarea>
                </div>
                
                    </div>
                    <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                    <input type="text" class="form-input" name="nickname" id="nickname" placeholder="Your Nickname"/>
                    </div>
                    <div class="form-group">
                    <input type="text" class="form-input" name="student_no" id="nickname" placeholder="Your Student Number"/>
                    </div>
                    <div class="form-group">
                    <select class="form-input" name="department">
                        <option value="year">Select Department</option>
                        <option value="year">Accounting & Finance</option>
                        <option value="year">Human Resource & Organizational Development </option>
                        <option value="year">Marketing & Corporate Strategy</option>
                        <option value="year">Supply Chain & Information Systems</option>
                    </select>
                    </div>
                    <!-- <div class="form-group">
                    <input type="text" class="form-input" name="nickname" id="nickname" placeholder="Your Nickname"/>
                    </div> -->
                    <div class="form-group">
                    <input type="submit" name="signup"  class="form-submit" value="Update Profile"/>
                    </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        </div>
    </section>
</main>