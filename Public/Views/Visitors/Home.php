<section id="hero">
            <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
                    <div class="carousel-inner" role="listbox">
                        <!-- Slide 1 -->
                        <div class="carousel-item active" style="background-image: url(<?=$ImagesDir?>/SystemImages/slide/slide1.jpg)">
                        <div class="carousel-container">
                                <div class="container">
                                 <h2 style="font-weight:300;" class="animate__animated animate__fadeInDown">WELCOME TO </h2>
                                <h2 class="animate__animated animate__fadeInUp"> KSBSA YEAR BOOK</h2>
                                <a href="<?=$BaseURL;?>/signup" class="btn-get-started animate__animated animate__fadeInUp scrollto">Join The Yearbook</a>
                                </div>
                            </div>
                        </div>

                        <!-- Slide 2 -->
                        <div class="carousel-item" style="background-image: url(<?=$ImagesDir?>/SystemImages/slide/slide2.jpg)">
                            <div class="carousel-container">
                                <div class="container">
                                  <h2 style="font-weight:300;" class="animate__animated animate__fadeInDown">WELCOME TO </h2>
                                <h2 class="animate__animated animate__fadeInUp"> KSBSA YEAR BOOK</h2>
                                <a href="<?=$BaseURL;?>/signup" class="btn-get-started animate__animated animate__fadeInUp scrollto">Join The Yearbook</a>
                                </div>
                            </div>
                        </div>

                        <!-- Slide 3 -->
                        <div class="carousel-item" style="background-image: url(<?=$ImagesDir?>/SystemImages/slide/slide3.jpg)">
                        <div class="carousel-container">
                                <div class="container">
                                   <h2 style="font-weight:300;" class="animate__animated animate__fadeInDown">WELCOME TO </h2>
                                <h2 class="animate__animated animate__fadeInUp"> KSBSA YEAR BOOK</h2>
                                <a href="<?=$BaseURL;?>/signup" class="btn-get-started animate__animated animate__fadeInUp scrollto">Join The Yearbook</a>
                                </div>
                            </div>
                        </div>

                    </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
            </a>

            </div>
    </section>

    <main id="main">
        <section id="search" class="search-bg1">
            <div>
                <h2 class="text-center text-white animate__animated animate__fadeInLeft"> Search For Your Mates <i class="fa fa-search pl-lg-5"></i></h2>
                
            </div>
        <div class="container mt-5">
            <div class="row d-flex justify-content-center">
                <div class="col-md-10">
                    <div class="card p-3 py-4">
                        <div class="row g-3 mt-2">
                        <div class="col-md-2">
                            <select name="year" id="ddlYears" class="form-control">
                            <option value="0">Year</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="department" id="department" class="form-control">
                                <option value="0">Department</option>
                                <option value="ACF">Accounting & Finance</option>
                                <option value="HROD">Human Resources & Organisational Development</option>
                                <option value="MCS">Marketing & Corporate Strategy</option>
                                <option value="SCIS">Supply Chain & Information Systems</option>
                            </select>    
                        </div>
                        <div class="col-md-6"> <input type="search" placeholder="Enter Full Name" class="form-control" >
                        </div>
                        <div class="col-md-2">     
                            <a href="<?=$BaseURL;?>/search" >
                             <button class="btn btn-secondary btn-block">Search</button> 
                            </a>
                        
                        </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
        </section>
        <section id="team" class="team section-bg">
        <div class="container">
            <div class="section-title">
            <h2>RECENTLY JOINED</h2>
            <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
            </div>
            <div class="row">
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member">
                <img src="<?=$ImagesDir?>/SystemImages/team/team-1.jpg" alt="">
                <h4>Walter White</h4>
                <p>
                    Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis perspiciatis quaerat qui aut aut aut
                </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member">
                <img src="<?=$ImagesDir?>/SystemImages/team/team-2.jpg" alt="">
                <h4>Sarah Jhinson</h4>
                <p>
                    Repellat fugiat adipisci nemo illum nesciunt voluptas repellendus. In architecto rerum rerum temporibus
                </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member">
                <img src="<?=$ImagesDir?>/SystemImages/team/team-3.jpg" alt="">
                <h4>William Anderson</h4>
                <p>
                    Voluptas necessitatibus occaecati quia. Earum totam consequuntur qui porro et laborum toro des clara
                </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member">
                <img src="<?=$ImagesDir?>/SystemImages/team/team-3.jpg" alt="">
                <h4>William Anderson</h4>
                <p>
                    Voluptas necessitatibus occaecati quia. Earum totam consequuntur qui porro et laborum toro des clara
                </p>
                </div>
            </div>
            </div>
        </div>
        </section>
    </main>